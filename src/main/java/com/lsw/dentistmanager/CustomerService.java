package com.lsw.dentistmanager;

import com.lsw.dentistmanager.entity.Customer;
import com.lsw.dentistmanager.enums.CustomerLevel;
import com.lsw.dentistmanager.model.CustomerItem;
import com.lsw.dentistmanager.model.CustomerRequest;
import com.lsw.dentistmanager.model.ReservationDateRequest;
import com.lsw.dentistmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer();
        addData.setChartNumber(request.getChartNumber());
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setCustomerIdNumber(request.getCustomerIdNumber());
        addData.setCustomerAddress(request.getCustomerAddress());
        addData.setFirstVisitDate(LocalDate.now());
        addData.setVisitPurpose(request.getVisitPurpose());
        addData.setChronicDisease(request.getChronicDisease());
        addData.setVisitRoute(request.getVisitRoute());
        addData.setEtc(request.getEtc());
        addData.setGender(request.getGender());

        customerRepository.save(addData);

    }

    public List<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();
        List<CustomerItem> result = new LinkedList<>();

        for(Customer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setChartNumber(item.getChartNumber());
            addItem.setCustomerInfo(
                    item.getCustomerName() + "/" +
                            item.getCustomerPhone()+ "/" +
                            item.getCustomerIdNumber()+ "/" +
                            item.getCustomerAddress());

            LocalDateTime timeStart = LocalDateTime.of(
                    item.getFirstVisitDate().getYear(),
                    item.getFirstVisitDate().getMonthValue(),
                    item.getFirstVisitDate().getDayOfMonth(),
                    0,
                    0,
                    0
            );

            LocalDateTime timeEnd = LocalDateTime.now();

            long dayCount = ChronoUnit.DAYS.between(timeStart, timeEnd);
            addItem.setCustomerLevel(dayCount + "일");


            CustomerLevel customerStatus = CustomerLevel.HUMAN;
            if (dayCount <= 60) {
                customerStatus = CustomerLevel.NEW;
            } else if (dayCount <= 365) {
                customerStatus = CustomerLevel.NORMAL;
            } else if (dayCount <= 730) {
                customerStatus = CustomerLevel.LONG;
            }
            addItem.setCustomerLevel(customerStatus.getName());

            addItem.setVisitPurpose(item.getVisitPurpose());
            addItem.setChronicDisease(item.getChronicDisease());
            addItem.setReservationDate(item.getReservationDate());
            addItem.setReservationTime(item.getReservationTime());
            addItem.setNonpayment(item.getNonpayment());
            addItem.setVisitRoute(item.getVisitRoute());
            addItem.setEtc((item.getEtc()));
            addItem.setGender(item.getGender().getName());
            //addItem

            result.add(addItem);
        }

        return result;
    }

    public void putReservationDate(long id, ReservationDateRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setReservationDate(request.getReservationDate());
        originData.setReservationTime(request.getReservationTime());

        customerRepository.save(originData);
    }




}
