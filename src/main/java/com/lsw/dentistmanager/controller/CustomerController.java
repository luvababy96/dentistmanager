package com.lsw.dentistmanager.controller;

import com.lsw.dentistmanager.CustomerService;
import com.lsw.dentistmanager.model.CustomerItem;
import com.lsw.dentistmanager.model.CustomerRequest;
import com.lsw.dentistmanager.model.ReservationDateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }

    @PutMapping("/reservation-date/id/{id}")
    public String putReservationDate(@PathVariable long id, @RequestBody @Valid ReservationDateRequest request) {
        customerService.putReservationDate(id,request);

        return "OK";
    }


}
