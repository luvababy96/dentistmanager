package com.lsw.dentistmanager.entity;

import com.lsw.dentistmanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,length = 20)
    private String chartNumber;

    @Column(nullable = false,length = 20)
    private String customerName;

    @Column(nullable = false,length = 20)
    private String customerPhone;

    @Column(nullable = false,length = 20)
    private String customerIdNumber;

    @Column(nullable = false,length = 40)
    private String customerAddress;

    @Column(nullable = false,length = 100)
    private String visitPurpose;

    @Column(length = 100)
    private String chronicDisease;

    @Column(nullable = false)
    private LocalDate firstVisitDate;

    private LocalDate reservationDate;

    @Column(length = 5)
    private String reservationTime;

    private Integer nonpayment;

    @Column(nullable = false,length = 10)
    private String visitRoute;

    @Column(length = 100)
    private String etc;

    @Column(nullable = false,length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;



}
