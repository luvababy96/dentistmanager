package com.lsw.dentistmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerLevel {

    NEW("신규고객"),
    NORMAL("일반고객"),
    LONG("장기고객"),
    HUMAN("휴면고객");

    private final String name;
}
