package com.lsw.dentistmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {
    MAN("남자","주민번호 1 혹은 3"),
    WOMAN("여자","주민번호 2 혹은 4");

    private final String name;
    private final String description;


}
