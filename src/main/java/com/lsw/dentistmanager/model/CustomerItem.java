package com.lsw.dentistmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {

    private Long id;

    private String chartNumber;

    private String customerLevel;

    private String customerInfo;

    private String visitPurpose;

    private String chronicDisease;

    private LocalDate reservationDate;

    private String reservationTime;

    private Integer nonpayment;

    private String visitRoute;

    private String etc;

    private String regularCheckUp;

    private String gender;


}
