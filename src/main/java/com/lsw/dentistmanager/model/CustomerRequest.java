package com.lsw.dentistmanager.model;

import com.lsw.dentistmanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerRequest {

    @NotNull
    @Length(min = 1,max = 20)
    private String chartNumber;

    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 7, max = 20)
    private String customerPhone;

    @NotNull
    @Length(min = 1,max = 14)
    private String customerIdNumber;

    @NotNull
    @Length(min = 1,max = 40)
    private String customerAddress;

    @NotNull
    @Length(min = 1,max = 100)
    private String visitPurpose;

    private String chronicDisease;

    @NotNull
    @Length(min = 1,max = 20)
    private String visitRoute;

    private String etc;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;



}
