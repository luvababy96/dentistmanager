package com.lsw.dentistmanager.repository;

import com.lsw.dentistmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
